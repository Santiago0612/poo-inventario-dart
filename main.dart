import 'dart:io';

class Producto {
  String nombre;
  int cantidad;
  double precio;

  Producto(this.nombre, this.cantidad, this.precio);

  void showDetails() {
    print('Nombre: $nombre');
    print('Cantidad: $cantidad');
    print('Precio: \$${precio.toStringAsFixed(2)}');
  }

  void sell(int cantidadVendida) {
    if (cantidadVendida <= cantidad) {
      cantidad -= cantidadVendida;
      print('$cantidadVendida unidades de $nombre vendidas.');
    } else {
      print('No hay suficientes unidades disponibles para vender.');
    }
  }
}


void main() {
  var inventario = <Producto>[];  // Lista para almacenar productos

  // Agregar productos a la lista inicial
  inventario.add(Producto(' macBockPro', 10, 15.99));
  inventario.add(Producto('Iphone X', 5, 29.99));

  // Interacción con el usuario para agregar productos
  bool continuar = true;
  while (continuar) {
    print('\nAgregar nuevo producto:');

    print('Nombre del producto:');
    var nombre = stdin.readLineSync();

    print('Cantidad:');
    var cantidadStr = stdin.readLineSync();
    var cantidad = int.parse(cantidadStr ?? '0');

    print('Precio:');
    var precioStr = stdin.readLineSync();
    var precio = double.parse(precioStr ?? '0');

    inventario.add(Producto(nombre!, cantidad, precio));

    print('¿Deseas agregar otro producto? (Sí/No)');
    var respuesta = stdin.readLineSync();
    continuar = respuesta?.toLowerCase() == 'si';
  }

  // Mostrar detalles de los productos en el inventario
  print('\nDetalles del inventario:');
  for (var producto in inventario) {
    producto.showDetails();
    print('-------------------------');
  }
}